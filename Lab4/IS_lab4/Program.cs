﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace IS_lab4
{
    class Program
    {
        static char[] alphabet = new char[72]
        {
            'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з',
            'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р',
            'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ',
            'ъ', 'ы', 'ь', 'э', 'ю', 'я', 'А', 'Б', 'В',
            'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К',
            'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У',
            'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь',
            'Э', 'Ю', 'Я', ' ', '.', ':', '!', '?', ','
        };
        static string inputPhrase = "";
        static int p;
        static int q;
        static int n;
        static int fi;
        static int e;
        static int d;
        static int[] inputPhraseCode;
        static int[] encryptedPhrase;
        static int[] decryptedPhraseCode;
        static string decryptedPhrase;
        static void Main(string[] args)
        {
            while (true)
            {
                while (true)
                {
                    Console.WriteLine("Введите фразу, которую необходимо зашифровать:");
                    inputPhrase = Console.ReadLine();
                    if (IsStringCorrect())
                        break;
                    else Console.WriteLine("\n---Фраза не удовлетворяет критериям алфавита!\n");
                }

                Console.WriteLine("Для шифрования необходимо ввести два простых неравных друг другу числа p и q. " +
                                  "\nРезультат произведения p и q не должен быть менее мощности алфавита, равной {0}", alphabet.Length);
                while (true)
                {
                    p = InputNumber("p");
                    q = InputNumber("q");
                    n = p * q;
                    if (n < alphabet.Length)
                        Console.WriteLine("\n---Результат произведения p и q меньше мощности алфавита, попробуйте еще раз");
                    else
                        break;
                }


                EncryptPhrase();
                Console.WriteLine("\nЗашифрованная последовательность:");
                Console.WriteLine(string.Join(" ", encryptedPhrase));

                DecryptPhrase();
                Console.WriteLine("\nДешифрированная последовательность:");
                Console.WriteLine(decryptedPhrase + "\n________________________________________");

                Console.WriteLine();
            }
        }

        static bool IsStringCorrect()
        {
            return inputPhrase.Length > 0 && inputPhrase.All(c => alphabet.Contains(c));
        }

        static int InputNumber(string varName)
        {
            int number;
            while (true)
            {
                Console.WriteLine("\nВведите {0} (простое число от 2):", varName);
                string input = Console.ReadLine();
                if (Int32.TryParse(input, out number))
                {
                    if (number > 0 && IsNumberSimple(number))
                    {
                        if ((varName == "q") && (number == p))
                            Console.WriteLine("\n---p и q не должны быть равными!\n");
                        else
                            break;
                    }
                    else
                        Console.WriteLine("\n---Введенное число не является простым!\n");
                }
                else
                    Console.WriteLine("\n---Введенное число не является целым!\n");
            }
            return number;
        }

        static bool IsNumberSimple(int number)
        {
            if (number == 1)
                return false;

            int numberSqrt = (int)Math.Pow(number, 0.5) + 1;

            for (int i = 2; i < numberSqrt; i++)
                if (number % i == 0)
                    return false;
            return true;
        }

        static void EncryptPhrase()
        {
            fi = (p - 1) * (q - 1);
            e = Calculate_e();
            d = Calculate_d();
            Console.WriteLine("\nn: " + n);
            Console.WriteLine("fi: " + fi);
            Console.WriteLine("e: " + e);
            Console.WriteLine("d: {0} \n", d);
            Console.WriteLine("Открытый ключ: {" + e + "; "+ n + "}");
            Console.WriteLine("Закрытый ключ: {" + d + "; " + n + "}");
            inputPhraseCode = inputPhrase.Select(c => Array.IndexOf(alphabet, c)).ToArray();
            encryptedPhrase = inputPhraseCode.Select(m => (int)(BigInteger.Pow(new BigInteger(m), d) % n)).ToArray();
        }

        static int Calculate_e()
        {
            int e = 2;
            while(BigInteger.GreatestCommonDivisor(e, fi) != 1)
            {
                e++;
            }
            return e;
        }

        static int Calculate_d()
        {
            int d = 2;
            while((d * e) % fi != 1)
            {
                d++;
            }
            return d;
        }

        static void DecryptPhrase()
        {
            decryptedPhraseCode = encryptedPhrase.Select(c => (int)(BigInteger.Pow(new BigInteger(c), e) % n)).ToArray();
            decryptedPhrase = string.Join("", decryptedPhraseCode.Select(c => alphabet[c]).ToArray());
        }
    }
}
