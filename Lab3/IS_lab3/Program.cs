﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Drawing2D;

namespace IS_lab3
{
    class Program
    {
        static char[] alphabet = new char[72]
        {
            'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з',
            'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р',
            'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ',
            'ъ', 'ы', 'ь', 'э', 'ю', 'я', 'А', 'Б', 'В',
            'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К',
            'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У',
            'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь',
            'Э', 'Ю', 'Я', ' ', '.', ':', '!', '?', ','
        };

        static string inputPhrase;
        static int[] inputPhraseCode;
        static string inputKey;
        static int keyMatrixDimension = 3;
        static double[,] keyMatrix;
        static double[,] keyReverseMatrix;
        static int[] encryptedPhrase;
        static string decryptedPhrase;

        static void Main(string[] args)
        {
            while (true)
            {
                while (true)
                {
                    Console.WriteLine("Введите фразу, которую необходимо зашифровать:");
                    inputPhrase = Console.ReadLine();
                    if (!CheckForInappropriateCharacter(inputPhrase) && inputPhrase.Length > 0)
                    {
                        TransformInput(ref inputPhrase, false);
                        break;
                    }

                    else Console.WriteLine("\nФраза не удовлетворяет критериям алфавита!\n");
                }
                while (true)
                {
                    Console.WriteLine("\nВведите ключ (9 символов):");
                    inputKey = Console.ReadLine();
                    if (!CheckForInappropriateCharacter(inputKey) && inputKey.Length > 0)
                    {
                        TransformInput(ref inputKey, true);
                        EncodeInputKey();
                        if(!CheckForMatrixDegeneration())
                            break;
                        else
                            Console.WriteLine("\nВведенный ключ приведет к вырождению матрицы ключа!\n");
                    }
                    else
                        Console.WriteLine("\nКлюч не удовлетворяет критериям алфавита!\n");
                }

                EncryptPhrase();
                Console.WriteLine("\nЗашифрованная последовательность:");
                Console.WriteLine(string.Join(" ", encryptedPhrase));

                DecryptPhrase();
                Console.WriteLine("\nДешифрированная последовательность:\n");
                Console.WriteLine(decryptedPhrase);

                Console.WriteLine();
            }
        }

        static bool CheckForInappropriateCharacter(string input)
        {
            return input.FirstOrDefault(c => !alphabet.Contains(c)) == '\u0000' ? false : true;
        }

        static void TransformInput(ref string input, bool isKey)
        {
            int validKeyValue = (int)Math.Pow(keyMatrixDimension, 2);
            int remainder = input.Length % keyMatrixDimension;
            StringBuilder stringBuilder = new StringBuilder(input);

            if (isKey && input.Length > validKeyValue)
                input = input.Remove(validKeyValue);
            else if(isKey && input.Length < validKeyValue)
            {
                stringBuilder.Append('*', validKeyValue - input.Length);
                input = stringBuilder.ToString();
            }
            else
            {
                stringBuilder.Append('*', remainder > 0 ? keyMatrixDimension - remainder : 0);
                input = stringBuilder.ToString();
            }
        }
        
        static void EncodeInputKey()
        {
            keyMatrix = new double[keyMatrixDimension, keyMatrixDimension];
            for (int rowNum = 0; rowNum < keyMatrixDimension; rowNum++)
            {
                for (int columnNum = 0; columnNum < keyMatrixDimension; columnNum++)
                {
                    keyMatrix[rowNum, columnNum] = Array.IndexOf(alphabet, inputKey[rowNum * keyMatrixDimension + columnNum]);
                }
            }
        }

        static bool CheckForMatrixDegeneration()
        {
            if (MatrixOperations.GetDeterminant(keyMatrix) == 0)
                return true;
            return false;
        }

        static void EncryptPhrase()
        {
            inputPhraseCode = inputPhrase.Select(c => Array.IndexOf(alphabet, c)).ToArray();
            encryptedPhrase = MatrixOperations.MatrixMultiplication(keyMatrix, inputPhraseCode);
        }
        static void DecryptPhrase()
        {
            keyReverseMatrix = MatrixOperations.Inverse(keyMatrix);
            decryptedPhrase = string.Join("", MatrixOperations.MatrixMultiplication(keyReverseMatrix, encryptedPhrase)
                              .Where(n => n >= 0)
                              .Select(n => alphabet[n]).ToArray());
        }
    }
}
