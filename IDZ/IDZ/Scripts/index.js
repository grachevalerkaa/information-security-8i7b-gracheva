﻿var inputText;

$(function () {
    $("#inputPhrase").focus(FocusFunction);
});

function FocusFunction() {
    if ($("#encryptedPhrase").val() != "") {
        $("#encryptedPhrase").val('');
        $("#decryptedPhrase").val('');
        $("input").val('');
    }
}

function IsStringCorrect() {
    inputText = $("#inputPhrase").val();
    $.get("Home/IsStringCorrect", { input: inputText }, function (data) {
        if (data == "False") {
            alert("Фраза не удовлетворяет критериям алфавита!")
        }
        else {
            Encrypt(inputText);
            $("#btnDecrypt").removeAttr("disabled");
        }
    });
}

function Encrypt(inputPhrase) {
    $.getJSON("Home/EncryptPhrase", { input: inputPhrase }, function (data) {
        $("#encryptedPhrase").val(data["EncryptedPhrase"]);
        $("#publicKey").val("{" + data["D"] + "; " + data["N"] + "}");
        $("#privateKey").val("{" + data["E"] + "; " + data["N"] + "}");
    });
}

function Decrypt() {
    $.get("Home/DecryptPhrase", function (data) {
        $("#decryptedPhrase").val(data);
    });
}

function Clear() {
    $("textarea").val('');
    $("input").val('');
    $("#btnDecrypt").attr("disabled", "disabled");
}