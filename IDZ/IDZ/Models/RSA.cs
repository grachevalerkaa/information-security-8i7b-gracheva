﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Numerics;

namespace IDZ.Models
{
    public static class RSA
    {
        static char[] alphabet = new char[72]
        {
            'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з',
            'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р',
            'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ',
            'ъ', 'ы', 'ь', 'э', 'ю', 'я', 'А', 'Б', 'В',
            'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К',
            'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У',
            'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь',
            'Э', 'Ю', 'Я', ' ', '.', ':', '!', '?', ','
        };

        static List<int> allNumbers;
        static List<int> simpleNumbers;
        static int p;
        static int q;
        public static int n { get; private set; }
        static int fi;
        public static int e { get; private set; }
        public static int d { get; private set; }
        static int[] inputPhraseCode;
        static int[] encryptedPhrase;
        static int[] decryptedPhraseCode;
        static string decryptedPhrase;

        public static bool IsStringCorrect(string input)
        {
            return input.Length > 0 && input.All(c => alphabet.Contains(c));
        }

        static void GetAllNumbers()
        {
            allNumbers = new List<int>();
            for (int i = 2; i < alphabet.Length * 2; i++)
                allNumbers.Add(i);
        }

        static void GetSimpleNumbers()
        {
            simpleNumbers = new List<int>();
            simpleNumbers = allNumbers;
            for(int i = 2; i < alphabet.Length * 2; i++)
            {
                for (int j = 0; j < simpleNumbers.Count; j++)
                {
                    if (simpleNumbers[j] != i && simpleNumbers[j] % i == 0)
                        simpleNumbers.RemoveAt(j);
                }
            }
        }

        static void GetPQ()
        {
            Random random = new Random();
            p = simpleNumbers[random.Next(simpleNumbers.Count)];
            while(q == p || q * p < alphabet.Length)
                q = simpleNumbers[random.Next(simpleNumbers.Count)];
        }

        public static int[] EncryptPhrase(string input)
        {
            GetAllNumbers();
            GetSimpleNumbers();
            GetPQ();
            n = p * q;
            fi = (p - 1) * (q - 1);
            e = Calculate_e();
            d = Calculate_d();
            inputPhraseCode = input.Select(c => Array.IndexOf(alphabet, c)).ToArray();
            encryptedPhrase = inputPhraseCode.Select(m => (int)(BigInteger.Pow(new BigInteger(m), d) % n)).ToArray();
            return encryptedPhrase;
        }

        static int Calculate_e()
        {
            int e = 2;
            while (BigInteger.GreatestCommonDivisor(e, fi) != 1)
            {
                e++;
            }
            return e;
        }

        static int Calculate_d()
        {
            int d = 2;
            while ((d * e) % fi != 1)
            {
                d++;
            }
            return d;
        }

        public static string DecryptPhrase()
        {
            decryptedPhraseCode = encryptedPhrase.Select(c => (int)(BigInteger.Pow(new BigInteger(c), e) % n)).ToArray();
            decryptedPhrase = string.Join("", decryptedPhraseCode.Select(c => alphabet[c]).ToArray());
            return decryptedPhrase;
        }
    }
}