﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IS_lab1
{
    class Program
    {
        static char[,] alphabet = new char[8, 9]
        {
            {'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з'},
            {'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р'},
            {'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ'},
            {'ъ', 'ы', 'ь', 'э', 'ю', 'я', 'А', 'Б', 'В'},
            {'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К'},
            {'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У'},
            {'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь'},
            {'Э', 'Ю', 'Я', ' ', '.', ':', '!', '?', ','},
        };
        static string inputPhrase;
        static List<int> encryptedPhrase;
        static string decryptedPhrase;

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Введите фразу, которую необходимо зашифровать:");
                inputPhrase = Console.ReadLine();

                EncryptPhrase();
                Console.WriteLine("Зашифрованная последовательность:");
                Console.WriteLine(string.Join("", encryptedPhrase));

                DecryptPhrase();
                Console.WriteLine("Дешифрированная последовательность:");
                Console.WriteLine(decryptedPhrase);

                Console.WriteLine();
            }
        }

        static void EncryptPhrase()
        {
            encryptedPhrase = new List<int>();
            for(int elemNumber = 0; elemNumber < inputPhrase.Length; elemNumber++)
            {
                SymbolSeek(inputPhrase[elemNumber]);
            }
        }

        static void DecryptPhrase()
        {
            decryptedPhrase = "";
            int rowNumber;
            int columnNumber;
            for (int elemNumber = 0; elemNumber < encryptedPhrase.Count()-1; elemNumber+=2)
            {
                rowNumber = encryptedPhrase[elemNumber] - 1;
                columnNumber = encryptedPhrase[elemNumber + 1] - 1;
                decryptedPhrase += alphabet[rowNumber, columnNumber];
            }
        }

        static void SymbolSeek(char symbol)
        {
            bool symbolIsFound = false;
            for(int rowNumber = 0; rowNumber < alphabet.GetLength(0) && !symbolIsFound; rowNumber++)
            {
                for(int columnNumber = 0; columnNumber < alphabet.GetLength(1); columnNumber++)
                {
                    if (symbol == alphabet[rowNumber, columnNumber])
                    {
                        encryptedPhrase.Add(rowNumber + 1);
                        encryptedPhrase.Add(columnNumber + 1);
                        symbolIsFound = true;
                        break;
                    }
                }
            }
        }
    }
}
