﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IS_lab3
{
    public class MatrixOperations
    {
        public static int[] MatrixMultiplication(double[,] A, int[] B)
        {
            int dimension = A.GetLength(0);
            double sum;
            int[] result = new int[B.Length];
            for (int i = 0; i < B.Length / dimension; i++)
            {
                for (int j = 0; j < dimension; j++)
                {
                    sum = 0;
                    for (int k = 0; k < dimension; k++)
                    {
                        sum += A[j, k] * B[i * dimension + k];
                    }
                    result[i * dimension + j] = Convert.ToInt32(sum);
                }
            }
            return result;
        }
        public static double GetDeterminant(double[,] A)
        {
            switch (A.GetLength(0))
            {
                case 2:
                    return A[0, 0] * A[1, 1] - A[0, 1] * A[1, 0];
                case 3:
                    return A[0, 0] * A[1, 1] * A[2, 2] +
                        A[1, 0] * A[2, 1] * A[0, 2] +
                        A[0, 1] * A[1, 2] * A[2, 0] -
                        A[0, 2] * A[1, 1] * A[2, 0] -
                        A[2, 1] * A[1, 2] * A[0, 0] -
                        A[1, 0] * A[0, 1] * A[2, 2];
                default:
                    throw new Exception("Неверный размер матрицы!"); ;
            }
        }
        static double[,] Transpose(double[,] A)
        {
            double[,] res = new double[A.GetLength(1), A.GetLength(0)];
            for (int i = 0; i < A.GetLength(0); i++)
            {
                for (int j = 0; j < A.GetLength(1); j++)
                {
                    res[j, i] = A[i, j];
                }
            }
            return res;
        }
        static double[,] UnionMatrix(double[,] A)
        {
            double[,] res = new double[A.GetLength(0), A.GetLength(1)];
            double[,] complement = new double[A.GetLength(0) - 1, A.GetLength(1) - 1];
            for (int i = 0; i < A.GetLength(0); i++)
            {
                for (int j = 0; j < A.GetLength(1); j++)
                {
                    int row = 0;
                    int col;
                    for (int k = 0; k < A.GetLength(0); k++)
                    {
                        if (k != i)
                        {
                            col = 0;
                            for (int l = 0; l < A.GetLength(1); l++)
                            {
                                if (l != j)
                                {
                                    complement[row, col] = A[k, l];
                                    col++;
                                }
                            }
                            row++;
                        }
                    }
                    res[i, j] = (int)Math.Pow(-1, i + j + 2) * GetDeterminant(complement);
                }
            }
            return res;
        }
        
        public static double[,] Inverse(double[,] A)
        {
            double det = GetDeterminant(A); //расчет определителя
            double[,] aT = Transpose(A); //транспонирование
            double[,] union = UnionMatrix(aT); //расчет союзной матрицы
            double[,] result = new double[A.GetLength(0), A.GetLength(1)];
            for (int i = 0; i < union.GetLength(0); i++) //расчет обратной матрицы
            {
                for (int j = 0; j < union.GetLength(1); j++)
                {
                    result[i, j] = (1.0 / det) * union[i, j];
                }
            }
            return result;
        }
    }
}
